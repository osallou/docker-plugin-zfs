try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Docker volume plugin for zfs',
    'author': 'Olivier Sallou',
    'url': 'https://bitbucket.org/osallou/docker-plugin-zfs',
    'download_url': '',
    'author_email': 'olivier.sallou@irisa.fr',
    'version': '1.0.0',
    'install_requires': ['nose',
                         'flask',
                         'subprocess32',
                         'humanfriendly',
                         'redis',
                         'gelfHandler',
                         'config'],
    'packages': find_packages(),
    'include_package_data': True,
    'scripts': ['docker-plugin-zfs.py'],
    'name': 'docker-plugin-zfs'
}

setup(**config)

