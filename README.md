# Requirements

Python: flask, config, subprocess32, humanfriendly, redis

Database: redis

Process owner must be root or have zfs command rights.

# Create device

https://thelinuxexperiment.com/create-a-virtual-hard-drive-volume-within-a-file-in-linux/

    fallocate -l 5G virtualdisk.img
    dd if=/dev/zero of=virtualdisk.img bs=1M count=5000
    mkfs -t ext4 virtualdisk.img
    sudo mkdir /mnt/zfsdisk
    sudo mount -t auto -o loop virtualdisk.img /mnt/zfsdisk
    # Check created device with df
    # Create zfs pool
    sudo zpool create godocker loop0

# Create zfs volumes

For information only, this is done by this plugin

    sudo zfs create godocker/test0
    sudo zfs set quota=100m godocker/test0
    ...
    sudo zfs destroy godocker/test0

# Usage

## Docker

    docker volume create --driver plugin-zfs --opt size=1G --name test1
    docker run -it --rm  --volume test1:/data xx yy
    docker volume rm test1

## Swarm

Swarm, on volume create, will create a volume on all nodes.
 
## Mesos

Plugin is triggered by HTTP following Docker volume plugin API

# Run

## Configuration

Configuration is in instance/dpzfs.cfg

## Development

    python docker-plugin-zfs.py

## Production

    gunicorn docker-plugin-zfs:app

## Systemd service

docker-plugin-zfs.service can be used for systemd init. Working dir must be adapted to match installation directory
