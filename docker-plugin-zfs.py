from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash, jsonify

import logging
import logging.config
import os
import sys
from config import Config
import time
import datetime
import humanfriendly
import json
import socket

if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess

import redis

app = Flask(__name__)
app.config['STATIC_FOLDER'] = 'app'
#app.config.from_object(default_config)

# Defaults
app.config["GLOBAL_POOL_SIZE"] = "5G"
app.config["ZPOOL"] = "godocker"
app.config["MOUNTPOINT"] = "/godocker"
app.config["REDIS_HOST"] = "localhost"
app.config["REDIS_PREFIX"] =  "docker-plugin-zfs"

config_filename = os.path.join(app.instance_path, 'dpzfs.cfg')
if os.path.exists(config_filename):
    app.config.from_pyfile(config_filename)

app.config.from_envvar('DPZFS_SETTINGS', silent=True)

handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
handler.setLevel(app.config['LOGGING_LEVEL'])
formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
handler.setFormatter(formatter)
app.logger.addHandler(handler)

app.logger.warn("Starting Docker volume plugin for zfs")
#logging.config.dictConfig(app.config['LOG_CONFIG'])

redis_client = redis.StrictRedis(host=app.config['REDIS_HOST'])

@app.route('/Plugin.Activate', methods=['GET', 'POST'])
def plugin_activate():
    hostname = socket.gethostname()
    initial_size = humanfriendly.parse_size(app.config["GLOBAL_POOL_SIZE"])
    redis_client.delete(app.config['REDIS_PREFIX']+':'+hostname)
    redis_client.incr(app.config['REDIS_PREFIX']+':'+hostname, initial_size)
    app.logger.warn("Active plugin-zfs plugin")
    app.logger.warn("Received: "+str(request.data))
    return jsonify({"Implements": ["VolumeDriver"]})

@app.route('/VolumeDriver.Create', methods=['POST'])
def plugin_create():
    app.logger.warn("Received: "+str(request.data))
    data = json.loads(request.data)
    opts = None
    if 'Opts' in data:
        opts = data['Opts']
    redis_client.set(app.config['REDIS_PREFIX']+':'+data['Name'],
        json.dumps({ "opts": opts,
                  "mount": os.path.join(app.config['MOUNTPOINT'], data['Name'])})
    )
    return jsonify({"Err": None})

@app.route('/VolumeDriver.Remove', methods=['POST'])
def plugin_remove():
    data = json.loads(request.data)
    app.logger.warn("Received: "+str(request.data))
    redis_client.delete(app.config['REDIS_PREFIX']+':'+data['Name'])
    return jsonify({"Err": None})


@app.route('/VolumeDriver.Mount', methods=['POST'])
def plugin_mount():
    hostname = socket.gethostname()
    data = json.loads(request.data)
    volume = redis_client.get(app.config['REDIS_PREFIX']+':'+data['Name'])
    if volume is None:
        return jsonify({"Err": "Volume not created "+data['Name']})
    volume = json.loads(volume)
    if volume['opts'] is None or not 'size' in volume['opts']:
        return jsonify({"Err": "Size not set in options"})

    requested_size = humanfriendly.parse_size(volume['opts']['size'])
    new_size = redis_client.decr(app.config['REDIS_PREFIX']+':'+hostname, requested_size)
    if new_size <= 0:
        redis_client.incr(app.config['REDIS_PREFIX']+':'+hostname, requested_size)
        return jsonify({"Err": "Not enough remaining storage"})

    status = subprocess.call(["zfs", "create",
                              app.config['ZPOOL']+'/'+data['Name']])
    if status != 0:
        return jsonify({"Err": "Failed to create storage"})
    redis_client.set(app.config['REDIS_PREFIX']+':mount:'+data['Name'],"1")
    status = subprocess.call(["zfs",
                              "quota="+str(volume['opts']['size']),
                              app.config['ZPOOL']+'/'+data['Name']])
    if status != 0:
        return jsonify({"Err": "Failed to create storage"})

    app.logger.warn("Received: "+str(request.data))
    return jsonify({"Err": None, "Mountpoint": volume['mount']})

@app.route('/VolumeDriver.Path', methods=['POST'])
def plugin_path():
    data = json.loads(request.data)
    app.logger.warn("Received: "+str(request.data))
    volume = redis_client.get(app.config['REDIS_PREFIX']+':'+data['Name'])
    if volume is None:
        return jsonify({"Err": "Volume not created "+data['Name']})
    volume = json.loads(volume)
    return jsonify({"Name": volume['mount']})

@app.route('/VolumeDriver.Unmount', methods=['POST'])
def plugin_unmount():
    hostname = socket.gethostname()
    data = json.loads(request.data)
    app.logger.warn("Received: "+str(request.data))
    is_mounted = redis_client.get(app.config['REDIS_PREFIX']+':mount:'+data['Name'])
    if is_mounted is None:
        return jsonify({"Err": None})
    status = subprocess.call(["zfs", "destroy",
                              app.config['ZPOOL']+'/'+data['Name']])
    if status != 0:
        app.logger.error("Failed to delete "+app.config['ZPOOL']+'/'+data['Name'])
        return jsonify({"Err": "Failed to create storage"})

    volume = redis_client.get(app.config['REDIS_PREFIX']+':'+data['Name'])
    if volume is None:
        return jsonify({"Err": "Volume not created "+data['Name']})
    volume = json.loads(volume)

@app.route('/info', methods=['GET'])
def info():
    info = subprocess.check_output(["zfs", "list"])
    return jsonify({"Err": None, "Info": info})


if __name__ == '__main__':
    debug = False
    if 'GODSTAT_ENV' in os.environ and \
      os.environ['GODSTAT_ENV']=='dev':
        debug = True
    app.run(host='0.0.0.0', debug=debug)
